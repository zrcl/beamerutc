PACKAGE_SRC = src

BUILD_ROOT=$(shell kpsewhich -var-value=TEXMFHOME)
BUILD_DIR = $(BUILD_ROOT)/tex/latex/utcRoberval_HF_MESRI

install: src/beamerthemeUTC.sty
	@echo $(BUILD_DIR)
	@mkdir -p $(BUILD_DIR)
	@cp -r $(PACKAGE_SRC)/* $(BUILD_DIR)
	@texhash $(BUILD_ROOT)
demo:
	@cd example;\
	mkdir -p build;\
	pdflatex -output-directory=build \\nonstopmode\\input example.tex;\
	pdflatex -output-directory=build \\nonstopmode\\input example.tex;\
	pdflatex -output-directory=build \\nonstopmode\\input example.tex;\
	mv build/example.pdf ./
clean:
	@rm -rf $(BUILD_DIR)
	@rm -rf example/build
	@texhash $(BUILD_DIR)
